<?php

namespace App\Models;

use Illuminate\Database\Eloquent\factories\hasFactory;
use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    use HasFactory;
    protected $table = 'menu';
    protected $fillable = ['gambar','judul','harga'];
}
